/*
 * This file is part of Mutator.
 *
 * Mutator is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Mutator is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Mutator.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TYPES_H
#define TYPES_H

typedef unsigned short int usi;

typedef short int s_int;

typedef struct s_word{
    usi size;
    char * word;
    struct s_word * next;
}s_word;

typedef struct{
    struct s_word * first;
    struct s_word * last;
    unsigned long count;
}s_list;


/*
 * Variables globales de apoyo al proceso de mutacion
 */
extern const char* strings[];
extern const char* specials[];

typedef struct{
    char orig;
    char muted;
}s_leet;

extern const s_leet leet[];

//Tipo definido de puntero a funcion
typedef void (*ptr_to_f_basics)(s_word * w);
typedef void (*ptr_to_f_adv)(s_list * l);

#endif
