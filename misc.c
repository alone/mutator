/*
 * This file is part of Mutator.
 *
 * Mutator is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Mutator is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Mutator.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "misc.h"

void 
list_new(s_list * list)
{
    list->first = NULL;
    list->last = NULL;
    list->count = 0;
}

short int 
list_add_item(s_list * list, s_word * word)
{
    s_word * new;

    new = (s_word *)amalloc(sizeof(s_word));
    new->word = (char *)amalloc(WORDSIZE);

    strncpy(new->word, word->word, (word->size>WORDSIZE)?WORDSIZE:word->size);
    new->size = word->size;
    new->next = NULL;

    if (list->count == 0){
        list->first = new;
        list->last = new;
    }
    else{
        list->last->next = new;
        list->last = new;
    }

    list->last->next = NULL;
    list->count++;

    return 0;
}

short int 
list_add_item_at_begin(s_list * list, s_word * word)
{
    s_word * new;

    new = (s_word *)amalloc(sizeof(word));
    new->word = (char *)amalloc(WORDSIZE);

    strncpy(new->word, word->word, word->size);
    new->size = word->size;
    new->next = NULL;

    new->next = list->first;
    list->first = new;

    if (list->count == 0)
        list->last = new;

    list->count++;

    return 0;
}

/*
 * Repasar, necesita una variable que controle si se ha encontrado o no un objeto igual.
 */
short int 
list_del_item(s_list * list, s_word * word)
{
    s_word * aux;

    if(list->count == 0){
        perror("Lista vacia");
        return -1;
    }

    aux = list->first;

    do{
        if(strcmp(aux->word, word->word)){
            word = aux;
            aux = aux->next;
            free(word);
            /*
             * Si solo queremos borrar una ocurrencia descomentar la siguiente linea.
             * return 1;
             */
        }
        //Recorre toda la lista, borrando todas las ocurrencias.
        aux = aux->next;
    }while(aux->next != NULL);

    list->count--;

    return 0;
}

short int 
list_del_item_first(s_list * list)
{
    s_word * aux;

    aux = list->first;
    list->first = list->first->next;
    free(aux->word);
    free(aux);

    list->count--;

    return 0;
}

short int 
list_del_item_last(s_list * list)
{
    s_word * aux;
    unsigned long i;

    aux = list->first;

    for(i=1;i<list->count;++i)
        aux = aux->next;

    free(list->last);
    list->last = aux;

    list->count--;
    return 0;
}

short int 
list_del_item_by_pos(s_list * list, unsigned long int pos)
{
    s_word * aux;
    s_word * tmp;
    unsigned long i;

    aux = list->first;

    for(i=0; i < list->count; i++){
        if(i == pos){
            tmp = aux;
            aux = aux->next;
            free(tmp);
            list->count--;
            return 0;
        }
        aux = aux->next;
    }

    return -1;
}
/*
inline short int 
list_count_items(s_list * list)
{
    return list->count;
}
*/
void 
list_destroy(s_list * list)
{
    while(list->count > 0)
        list_del_item_first(list);
}

/*
 * Return -1 is the item is not found, otherwise return the position of first ocurrence
 */
short int 
list_find_item(s_list * list, s_word * waux)
{
    unsigned long i;
    s_word * windex;

    windex = list->first;
    for(i=0;i<list->count;i++){
        if (strcmp(waux->word,windex->word) == 0)
            return i;
        windex = windex->next;
    }

    return -1;
}

short int 
list_concatenate(s_list* list, s_list* laux)
{
    s_word * waux;
    unsigned long i;

    waux = laux->first;
    for (i=0;i<laux->count;i++){
        if(list_find_item(list,waux) < 0)
            list_add_item(list,waux);
        waux = waux->next;
    }

    return 0;
}

void * 
amalloc(size_t size)
{
    void * pt;

    pt = malloc(size);

    if (pt == NULL){
        printf("ERROR: no memory is available!!!");
        exit(-1);
    }

    memset(pt, '\0', size);

    return pt;
}

char *
astrdup (const char *word)
{
  char * aux;

  aux =(char*)amalloc (strlen(word) + 1);
  strncpy (aux, word, strlen(word));
  
  return aux;
}
