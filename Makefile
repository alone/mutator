PACKAGE = mutator
VERSION = 0.2
GCCVERSION := $(shell expr `gcc -dumpversion | cut -d. -f1,2 | sed 's/\.//g'` \>= 46)
OSTYPE := $(shell uname -s)

CFLAGS = -I. -O3 -Wall -Wextra -g -pedantic

ifneq ($(OSTYPE),Linux)
CFLAGS += -std=c99
else
    ifeq "$(GCCVERSION)" "1"
	CFLAGS += -std=c11
    else
    	CFLAGS += -std=c99
    endif
endif

OBJ = misc.o main.o mutator.o
BIN = $(PACKAGE)

$(PACKAGE): $(OBJ)
	$(CC) $(CFLAGS) -o $(BIN) $(OBJ)

main.o: 
	$(CC) $(CFLAGS) -c main.c
misc.o: 
	$(CC) $(CFLAGS) -c misc.c
mutator.o: mutator.h
	$(CC) $(CFLAGS) -c mutator.c

release:
	$(shell git archive master --prefix='mutator/' --format=tar.gz > ../release/mutator_release-`git describe master`.tar.gz)

.PHONY: clean
clean:
	rm -f $(OBJ)  $(BIN)
	$(shell if [ -f output.dic ];then rm -f output.dic;fi)
