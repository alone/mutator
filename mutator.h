/*
 * This file is part of Mutator.
 *
 * Mutator is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Mutator is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Mutator.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MUTATOR_H
#define MUTATOR_H

#include"main.h"
#include"misc.h"

/*
 * Funciones de mutado de palabras
 */
void upper_case(s_word * w);
void lower_case(s_word * w);
void swap_case(s_word * w);
void first_char_upper(s_word * w);
void last_char_upper(s_word *w);
void first_last_char_upper(s_word *w);
void to_leet(s_list * l);
void add_special_chars(s_list * l);
void append_string(s_list * l);
//void prepend_string(s_list * l);
//void append_int(s_list * l);
//void prepend_int(s_list *l);
void add_year(s_list * l);

static void curyear(char *);
//NOP function
void nop(s_list * l);

// Declaracion de la tabla de punteros a funciones
ptr_to_f_basics p_table_basics[7] = {upper_case, lower_case, first_char_upper, last_char_upper,first_last_char_upper, swap_case, NULL};
ptr_to_f_adv p_table_adv[7] = {add_special_chars, append_string/*, prepend_string, append_int, prepend_int*/, add_year, NULL};

#endif
