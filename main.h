/*
 * This file is part of Mutator.
 *
 * Mutator is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Mutator is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Mutator.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MAIN_H
#define MAIN_H

#define _GNU_SOURCE

#include<stdio.h>
#include<ctype.h>
#include<fcntl.h>
#include<stdlib.h>
#include<string.h>
#include<unistd.h>
#include<time.h>

#include<getopt.h>

#define PROGRAM     "Mutator"
#define VERSION     "v0.2"
#define AUTHOR      "@AloneInTheShell"
#define EMAIL       "alone.in.the.shell@gmail.com"
#define WORDSIZE    50
#define OUTPUT      "output.dic"
#define TRUE        1
#define FALSE       0
#define MAX(x,y)    ((x)>(y)?(x):(y))
#define MIN(x,y)    ((x)<(y)?(x):(y))
#define LEN(x)      (sizeof(x)/sizeof(x[0]))

#include "types.h"
#include "misc.h"

struct s_opts {
	usi has_output;
	usi has_file;
	usi has_word;
	usi do_basic;
	int do_adv;
	usi no_spc;
	usi no_str;
	usi no_year;
	usi has_yrange;
	char year_range[WORDSIZE];
}options;

/*
 * Usage
 */
void usage(void) __attribute__((noreturn));

/*
 * Lee linea a linea el fichero dado
 */
int read_word(int fd, s_word * w);

void write_word(int fd, s_word * w);

/*
 * Realiza las mutaciones pedidas
 */
void mutator(s_list* l);

#endif
