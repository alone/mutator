/*
 * This file is part of Mutator.
 *
 * Mutator is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Mutator is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Mutator.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * Fichero que implementa listas enlazadas.
 */

#ifndef MISC_H
#define MISC_H

#include"main.h"

/*
 * Operaciones con listas
 */
void list_new(s_list * list);
short int list_add_item(s_list* list, s_word * word);
short int list_add_item_at_begin(s_list* list, s_word * word);
short int list_del_item(s_list* list, s_word * word);
short int list_del_item_first(s_list* list);
short int list_del_item_last(s_list* list);
short int list_del_item_by_pos(s_list* list, unsigned long int pos);
//inline short int list_count_items(s_list* list);
void * list_get_item(s_list* list, s_word * word);
void * list_get_item_by_pos(s_list* list, unsigned long int pos);
void list_destroy(s_list* list);
short int list_find_item(s_list* list, s_word* waux);
short int list_concatenate(s_list* list, s_list* laux);

/*
 * Miscelanea
 */
void * amalloc(size_t size);
char * astrdup(const char *word);

#endif
