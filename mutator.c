/*
 * This file is part of Mutator.
 *
 * Mutator is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Mutator is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Mutator.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "mutator.h"

/*
 * Variables globales de apoyo al proceso de mutacion
 */
const char* strings[] = {"admin","sys","pw","pwd","adm","srv","1","123","1234"};
const char* specials[] = {"$","_","!",".","%","-","@","#","*","&","|","+"/*," "*/};
const s_leet leet[] = { {'a','4'},
                       {'e','3'},
                       {'i','1'},
                       {'o','0'}};
//                       {'s','5'}};
//                       {'s','$'}};

void 
mutator(s_list * list)
{
    unsigned long i,j;
    s_word * word;
    s_word waux;
    s_list laux;

    list_new(&laux);

    waux.word = (char*)amalloc(WORDSIZE);

    printf("[+] Current word: '%s'\n", list->first->word);

    if(! options.do_adv )
    {
        printf("\t[-] Basics mutations generated: ");
        //Basics Mutations
        for(i=0;p_table_basics[i]!=NULL;i++)
        {
            word = list->first;
            for(j=0;j<list->count;j++)
            {
                waux.size = word->size;
                strncpy(waux.word, word->word, MIN(word->size,WORDSIZE));
                p_table_basics[i](&waux);
                list_add_item(&laux, &waux);
                word = word->next;
            }
        }
        printf("%ld\n",laux.count);
        list_concatenate(list,&laux);
    }

    free(waux.word);
    list_destroy(&laux);

    //To_leet mutation.
    to_leet(list);

    //TEST
    if (options.no_spc)
        p_table_adv[0]=nop;

    if (options.no_str)
        p_table_adv[1]=nop;

    if (options.no_year)
        p_table_adv[2]=nop;

    if (! options.do_basic ){
        //Advanced Mutations
        for (i=0;p_table_adv[i]!=NULL;i++)
            p_table_adv[i](list);
    }

}

/*
 * Funciones de mutado de palabras
 */
void 
upper_case(s_word *w)
{
    usi i;

    for(i=0; i<w->size; i++)
    {
        if((isalpha(*(w->word+i)) != 0)&&(isupper(*(w->word+i)) == 0))
             *(w->word+i) = toupper(*(w->word+i));
    }
}

void 
lower_case(s_word *w)
{
    usi i;

    for(i=0; i<w->size; i++)
    {
        if((isalpha(*(w->word+i)) != 0)&&(islower(*(w->word+i)) ==0))
             *(w->word+i) = tolower(*(w->word+i));
    }
}

void 
swap_case(s_word *w)
{
    usi i;

    for(i=0; i<w->size; i++)
    {
        if(isalpha(*(w->word+i)) != 0)
        {
            if(isupper(*(w->word+i))!=0)
                *(w->word+i) = tolower(*(w->word+i));
            else
                *(w->word+i) = toupper(*(w->word+i));
        }
    }
}

void 
first_char_upper(s_word *w)
{
    if((isalpha(*(w->word)) != 0)&&(isupper(*(w->word)) == 0))
        *(w->word) = toupper(*(w->word));
}

void 
last_char_upper(s_word *w)
{
    usi i = w->size -1;

    if((isalpha(*(w->word+i)) != 0)&&(isupper(*(w->word+i)) == 0))
        *(w->word+i) = toupper(*(w->word+i));
}

void 
first_last_char_upper(s_word *w)
{
    first_char_upper(w);
    last_char_upper(w);
}

void 
to_leet(s_list * list)
{
    usi j,z;
    unsigned long i;
    s_list laux;
    s_word * windex;
    s_word * waux;

    //laux = (s_list*)amalloc(sizeof(s_list));
    waux = (s_word*)amalloc(sizeof(s_word));
    waux->word = (char *)amalloc(WORDSIZE);

    list_new(&laux);

    windex = list->first;
    printf("\t[-] To leet mutations generated: ");
    for(i=0;i<list->count;i++)
    {
        waux->size = windex->size;
        strncpy(waux->word, windex->word,windex->size);
        for(j=0;j<windex->size;j++)
        {
            for(z=0;z<LEN(leet);z++)
            {
                if(*(windex->word+j) == leet[z].orig)
                    *(waux->word+j) = leet[z].muted;
            }
        }
        list_add_item(&laux, waux);
        windex = windex->next;
        memset(waux->word,'\0', WORDSIZE);
    }
    printf("%ld\n",laux.count);
    list_concatenate(list, &laux);
    free(waux->word);
    free(waux);
    list_destroy(&laux);
    //free(laux);
}

void 
add_special_chars(s_list * list)
{
    usi i;
    unsigned long j;
    s_word * waux;
    s_word * dest;
    s_list laux;

    //laux = (s_list*)amalloc(sizeof(s_list));
    dest = (s_word*)amalloc(sizeof(s_word));
    dest->word = (char*)amalloc(WORDSIZE);
    list_new(&laux);

    printf("\t[-] Special chars mutations generated: ");
    for(i=0;i<LEN(specials);i++)
    {
        waux = list->first;
        for(j=0;j<list->count;j++)
        {
            if(waux->size < WORDSIZE)
            {
                sprintf(dest->word,"%s%s\n",waux->word,specials[i]);
                dest->size = strlen(waux->word)+strlen(specials[i]);
                list_add_item(&laux, dest);
            }
            else
                printf("[*] The word \"%s\" exceed the maximun size allowed!\n", waux->word);
            waux = waux->next;
        }
    }
    printf("%ld\n",laux.count);
    list_concatenate(list, &laux);

    free(dest->word);
    free(dest);
    list_destroy(&laux);
}

void 
append_string(s_list * list)
{
    usi i;
    unsigned long j;
    s_word * waux;
    s_word * dest;
    s_list laux;

    //laux = (s_list*)amalloc(sizeof(s_list));
    dest = (s_word*)amalloc(sizeof(s_word));
    dest->word = (char*)amalloc(WORDSIZE);
    list_new(&laux);

    printf("\t[-] Append strings mutations generated: ");
    for(i=0;i<LEN(strings);i++)
    {
        waux = list->first;
        for(j=0;j<list->count;j++)
        {
            if(waux->size < WORDSIZE)
            {
                sprintf(dest->word,"%s%s\n",waux->word,strings[i]);
                dest->size = strlen(waux->word)+strlen(strings[i]);
                list_add_item(&laux, dest);
            }
            else
                printf("[*] The word \"%s\" exceed the maximun size allowed!\n", waux->word);
            waux = waux->next;
        }
    }

    printf("%ld\n",laux.count);
    list_concatenate(list, &laux);

    free(dest->word);
    free(dest);
    list_destroy(&laux);
}

/* This mutation is not usual, IMHO, but feel free
 * to uncomment it, don't forget uncomment it in .h file too.
void 
prepend_string(s_list * list)
{
    usi i,j;
    s_word * waux;
    s_word * dest;
    s_list * laux;

    laux = (s_list*)amalloc(sizeof(s_list));
    //waux = (s_word*)amalloc(sizeof(s_word));
    dest = (s_word*)amalloc(sizeof(s_word));
    dest->word = (char*)amalloc(WORDSIZE);
    list_new(laux);

    printf("\t[-] Prepend strings mutations generated: ");
    for(i=0;i<LEN(strings);i++)
    {
        waux = list->first;
        for(j=0;j<list->count;j++)
        {
            if(waux->size < WORDSIZE)
            {
                sprintf(dest->word,"%s%s\0",strings[i],waux->word);
                dest->size = strlen(waux->word)+strlen(strings[i]);
                list_add_item(laux, dest);
            }
            waux = waux->next;
        }
    }

    printf("%d\n",laux->count);
    list_concatenate(list, laux);

    free(dest->word);
    free(dest);
//    free(waux);
}
*/

void 
add_year(s_list * list)
{
    unsigned long j=0;
    unsigned long i=0;
    char buf[5];
    char startyear[5];
    char endyear[5];
    int start;
    int end;
    s_word * waux;
    s_word * dest;
    s_list laux;

    memset(buf,'\0',5);
    memset(startyear,'\0',5);
    memset(endyear,'\0',5);
	
    curyear(buf);
    if (options.has_yrange)
	    i=sscanf(options.year_range, "%[^-]-%s", startyear, endyear);

    end=atoi(i==2?endyear:buf);

    //laux = (s_list*)amalloc(sizeof(s_list));
    dest = (s_word*)amalloc(sizeof(s_word));
    dest->word = (char*)amalloc(WORDSIZE);
    list_new(&laux);

    waux = list->first;
    printf("\t[-] Append year mutations generated: ");
    for(j=0;j<list->count;j++)
    {
        if((waux->size+strlen(buf)) < WORDSIZE)
        {
		for(start=atoi(startyear);start<=end;start++){
			sprintf(dest->word,"%s%d\n",waux->word,start);
			dest->size = waux->size+strlen(startyear);
			list_add_item(&laux, dest);
		}
	}
	else
		printf("[*] The word \"%s\" exceed the maximun size allowed!\n", waux->word);
	waux = waux->next;
    }

    printf("%ld\n",laux.count);
    list_concatenate(list, &laux);

    free(dest->word);
    free(dest);
    list_destroy(&laux);
}

static void
curyear(char * buf)
{
	time_t now;
	struct tm * curtime;

	time(&now);
	curtime = localtime(&now);
	(void)strftime(buf,sizeof(buf),"%Y",curtime);

	return;
}

void
nop(s_list* l){/*NOP FUNCTION*/}
