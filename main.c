/*
 * This file is part of Mutator.
 *
 * Mutator is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Mutator is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Mutator.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * <-[ Mutator v0.1 ]->
 * 
 * Autor: @AloneInTheShell
 * Mail: alone.in.the.shell@gmail.com
 *
 * Este soft ha sido creado ante la imposiblidad de encontrar un 
 * mutador de diccionarios que se adaptara a mis necesidades, con lo
 * que posiblemente NO cumpla las tuyas ;P
 * Por defecto realiza todas las mutaciones definidas y segun las
 * opciones que pongas deshabilitas unas u otras.
 * Una de las principales caracteristicas frente a otros mutadores
 * de diccionarios es que aplica determinadas "mutaciones" a los
 * resultados de pruebas anteriores, permitiendo generar passwords
 * del tipo: 
 * corporation -> C0rp0r4t10n_2012
 * Lo cual no he encontrado en ningun otro programa del estilo.
 *
 * Usage:
 * Syntax: mutator [options] wordlist
 *
 * Options:
 *  -v, --version           Show version information
 *  -h, --help              Show this help
 *  -o, --output [file]     File to write the results
 *  -f, --file [file]*      File from read the words
 *  -w, --word [word]*      Word to mutate
 *  -b, --basic             Only "case" and "l33t" mutations
 *  -a, --advanced          Only advanced mutations
 *  -y, --years [year]      No append,prepend year, if a year is specified \
 *                          append range between year specified and actual year
 *  -x, --specials          No append specials chars
 *  -s, --strings           No append,prepend hardcoded strings
 *
 *  One of these options -w or -f is required
 *
 */

#include "main.h"

int 
main (int argc, char ** argv)
{
    char output[WORDSIZE];
    char input[WORDSIZE];
    char opt;
    unsigned long i, j, total_mutations = 0;
    int fdin, fdout,s;
    s_list list,list_out;
    s_word aux, * word = &aux, * word_out = &aux;

    list_new(&list);

    memset(output, '\0', WORDSIZE);
    memset(input, '\0', WORDSIZE);
    memset(options.year_range, '\0', WORDSIZE);

    struct option longopts[] = {
        {"advanced", no_argument, (int *) &options.do_adv, 1},
        {"basic", no_argument, (int *) &options.do_basic, 1},
        {"word", required_argument, 0, 'w'},
        {"special", no_argument,(int *) &options.no_spc, 1},
        {"strings", no_argument,(int *) &options.no_str, 1},
        {"years", optional_argument, 0, 'y'},
        {"output", required_argument, 0, 'o'},
        {"file", required_argument, 0, 'f'},
        {"version", no_argument, 0, 'v'},
        {"help", no_argument, 0, 'h'},
        {0, 0, 0, 0}
    };

    while((opt=getopt_long(argc, argv, ":vo:bayxsw:hf:", longopts, NULL))!= -1)
    {
        switch(opt)
        {
            case 'v':
                printf("%s %s - by %s\n",PROGRAM,VERSION,AUTHOR);
                exit(-1);
                break;
            case 'o':
                if (optarg)
                {
                    options.has_output=TRUE;
                    strncpy(output, optarg, MIN(strlen(optarg),WORDSIZE));
                }
                break;
            case 'b':
                options.do_basic=TRUE;
                break;
            case 'a':
                options.do_adv=TRUE;
                break;
            case 'y':
		if(optarg){	
			options.has_yrange=TRUE;
			strncpy(options.year_range, optarg, MIN(strlen(optarg),WORDSIZE));
		} else
                	options.no_year=TRUE;
                break;
            case 'x':
                options.no_spc=TRUE;
                break;
            case 's':
                options.no_str=TRUE;
                break;
            case 'w':
                options.has_word=TRUE;
                strncpy(input, optarg, MIN(strlen(optarg),WORDSIZE));
                break;
            case 'f':
                options.has_file=TRUE;
                strncpy(input, optarg, MIN(strlen(optarg),WORDSIZE));
                break;
            case 0:
                break;
            case ':':
            case '?':
                printf("Unknow option or no option argument specificed.\n");
            default :
            case 'h':
                usage();
                break;
        }
    }

    if (!options.has_output)
        strncpy(output, OUTPUT, strlen(OUTPUT));

    if((fdout=open(output, O_WRONLY|O_TRUNC|O_CREAT, 0664)) == -1){
        perror(output);
        exit(-1);
    }

    if (options.has_word){
        word->word= input;
        word->size= strlen(input);
        list_add_item(&list, word);
    }
    else if(options.has_file){
        if((fdin=open(input, O_RDONLY)) == -1){
            perror(input);
            exit(-1);
        }

        while((s=read_word(fdin, word)) !=0){
            list_add_item(&list, word);
            free(word->word);
        }

        close(fdin);
    }
    else
        usage();

    printf("[+] Number of words to mutate: %ld\n",list.count);

    word_out = list.first;
    for (j=0;j<list.count; j++){
	list_new(&list_out);
	list_add_item(&list_out, word_out); 
	
	mutator(&list_out);

	word = list_out.first;
	for (i=0;i<list_out.count;i++){
		write_word(fdout, word);        
		word = word->next;
	}
	
	total_mutations += list_out.count;

	word_out = word_out->next;

	list_destroy(&list_out);
    }

    list_destroy(&list);
    close(fdout);

    printf("[+] Total mutations generated: %ld\n", total_mutations);

    return 0;
}

void 
usage(void)
{
    char * help="\
\nSyntax: mutator [options] wordlist\
\n\nOptions:\
\n\t-v, --version      \tShow version information\
\n\t-h, --help         \tShow this help\
\n\t-o, --output [file]\tFile to write the results\
\n\t-f, --file [file]* \tFile from read the words\
\n\t-w, --word [word]* \tWord to mutate\
\n\t-b, --basic        \tOnly \"case\" and \"l33t\" mutations\
\n\t-a, --advanced     \tOnly advanced mutations\
\n\t-y, --years=[year] \tNo append,prepend year, if a year is specified append\
range between year specified and actual year, you can specified a range as well [year-year]\
\n\t-x, --specials     \tNo append specials chars\
\n\t-s, --strings      \tNo append,prepend hardcoded strings\
\n\n\tOne of these options -w or -f is required";
    
    printf("%s %s by %s email:<%s>\n", PROGRAM, VERSION, AUTHOR, EMAIL);
    printf("%s\n\n",help);

    exit(-1);
}

int 
read_word(int fd, s_word * w)
{
    char c[WORDSIZE];
    int i=-1;
    short int s;

    memset(c,'\0',WORDSIZE);

    do
    {
        i++;
        s=read(fd, &c[i],sizeof(char));
   // }while((c[i]!='\n')&&(s!=0));    
    }while(!isblank(c[i]) && !isspace(c[i]) && s!= -1 && i < WORDSIZE-1);    
    
    //Yes, I know, this is a fucking piece of shit, but enough while implement
    //a decent trim function ;P
    if (c[i]=='\r')
        lseek(fd, 1, SEEK_CUR);

    w->word = astrdup(c);
    w->size = i;
    
    return s;
}

void 
write_word(int fd, s_word * w)
{
    int s;

    if ((s=write(fd,w->word,w->size)) == -1){
        perror("Write error!");
        exit(-1);
    }

    if ((s=write(fd, "\n",1)) == -1){
	perror("Write error!");
	exit(-1);
    }
}
